// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////

#include <cstdio>
#include <math.h>  
#include "../common/PsbPidController.h"
#include "SwerveModule.h"
#include "../common/DegreeCalc.h"

#include <iostream>
#include <string>


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Drivebase {

////////////////////////////////////////////////////////////////////////////////
SwerveModule::SwerveModule(
   double x,
   double y,
   PsbPidController * rotatePIDController) 
   :
   m_desired_angle(0),
   m_x(x),
   m_y(y),
   m_drive_motor(0),
   m_rotate_motor(0),
   m_rotate_encoder_counts(0),
   m_rotatePID(rotatePIDController)
{

   // intentionally left empty

}

SwerveModule::SwerveModule(void) 
   : 
   m_desired_angle(0),
   m_drive_motor(0),
   m_rotate_motor(0),
   m_rotate_encoder_counts(0)
{
   
   // intentionally left empty

}

////////////////////////////////////////////////////////////////////////////////
PsbPidController *
SwerveModule::createPID(
      float kp,
      float ki,
      float kd,
      float min_out,
      float max_out,
      float minIntegralSum,
      float maxIntegralSum,
      float innerEpsilon,
      float outerEpsilon)
{
 return new PsbPidController(
                                    kp
                                 ,  ki
                                 ,  kd
                                 ,  0
                                 ,  0    
                                 ,  360   
                                 ,  min_out  
                                 ,  max_out  
                                 ,  minIntegralSum     
                                 ,  maxIntegralSum     
                                 ,  innerEpsilon   
                                 ,  outerEpsilon   
                                 ,  true
   );

}


////////////////////////////////////////////////////////////////////////////////
SwerveModule::~SwerveModule(void)   
{
   // Intentionally left empty
}


////////////////////////////////////////////////////////////////////////////////
double
SwerveModule::getCurrentAngle(bool acountForReversed)
{
   double angle = DegreeCalc::normalizeDegree((double) (m_rotate_encoder_counts - m_forward_tick_value) * m_ticks_per_revolution_mulitpier);


   if (acountForReversed)
   {
      return m_reversed ? 
      DegreeCalc::normalizeDegree((double) angle - 180) 
      : 
      angle;
   }
   else
   {
      return angle;
   }
   

}

////////////////////////////////////////////////////////////////////////////////
void
SwerveModule::enable()
{
   m_enabled = true;
   m_rotatePID->enable();//don't really need to do this
}

////////////////////////////////////////////////////////////////////////////////
void
SwerveModule::disable()
{
   m_enabled = false;

   m_rotatePID->disable();//don't really need to do this
   m_drive_motor = 0;
   m_rotate_motor = 0;

}


////////////////////////////////////////////////////////////////////////////////
double
SwerveModule::getX(void)
{
   return m_x;
}

////////////////////////////////////////////////////////////////////////////////
double
SwerveModule::getY(void)
{
   return m_y;
}

////////////////////////////////////////////////////////////////////////////////
double
SwerveModule::getDriveMotor(void)
{
   return m_drive_motor;
}

////////////////////////////////////////////////////////////////////////////////
double
SwerveModule::getRotateMotor(void)
{
   return m_rotate_motor;
}

////////////////////////////////////////////////////////////////////////////////
void
SwerveModule::setTickValueForForward(double ticks)
{
   m_forward_tick_value = ticks;
}


////////////////////////////////////////////////////////////////////////////////
void
SwerveModule::setTicksPerRotation(double ticksInOneRotation)
{
   m_ticks_per_revolution_mulitpier = 360 / ticksInOneRotation;
}


////////////////////////////////////////////////////////////////////////////////
void
SwerveModule::setRotateEncoderCounts(double newValue)
{
   m_rotate_encoder_counts = newValue;
}


////////////////////////////////////////////////////////////////////////////////
double
SwerveModule::getDesiredAngle(void)
{
   return m_desired_angle;
}

////////////////////////////////////////////////////////////////////////////////
void
SwerveModule::setVelocity(double drive_throttle, double new_angle)
{
   m_desired_angle = DegreeCalc::normalizeDegree(new_angle);//set new angle


   // if were not enabled do nothing!
   if (!m_enabled)
   {
      m_drive_motor = 0;
      m_rotate_motor = 0;
      return;
   }


   //--------------------------------
   // Calculate Rotate Motor Output
   //-------------------------------

   double angleDiff = DegreeCalc::getAngleDistance(getCurrentAngle(), m_desired_angle);
   double angleReversedDiff = DegreeCalc::getAngleDistance(DegreeCalc::normalizeDegree(getCurrentAngle() - 180), m_desired_angle);


   if (angleDiff >= angleReversedDiff) // if just rotating normally is farther than reversing then reverse!
   {
   
      //swap everything                    
      if (m_reversed)
      {
         m_reversed = false;                        
      }                    
      else if (!m_reversed)
      {
         m_reversed = true;
      }
      
      //reset pid?
      
   }


   
   // apply rotate to motor with pid
   m_rotatePID->setSetPoint(m_desired_angle);
   m_rotate_motor = m_rotatePID->calcPid(getCurrentAngle());
   // printf("Offset:%.3f\n", getCurrentAngle() + getPidAngleOffset(m_desired_angle, getCurrentAngle()));



   //-------------------------------
   // Calculate Drive Motor Output
   //-------------------------------
         
   if (m_reversed)
   {
      m_drive_motor = drive_throttle * -1;
   }      
   else
   {
      m_drive_motor = drive_throttle;
   }
      
   
   
 
}
 



}
} // END namespace Psb

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

