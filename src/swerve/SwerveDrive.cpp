// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////

#include "SwerveModule.h"
#include "SwerveDrive.h"
#include "../common/DegreeCalc.h"
#include <string> // might not need this

#include <iostream>
#include <vector>





// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////
#define M_PI         3.14159265358979323846  // pi

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Drivebase {



////////////////////////////////////////////////////////////////////////////////
SwerveDrive::SwerveDrive(bool enabled, std::vector<Psb::Drivebase::SwerveModule*> modules) :
      m_enabled(enabled)
{

   // not sure why I cant just say vector = vector but adding each module works too
   for(auto module : modules)
   {
      m_modules.push_back(module);
   }

}

////////////////////////////////////////////////////////////////////////////////
SwerveDrive::SwerveDrive(void)
{
   // Intentionally left empty
}

////////////////////////////////////////////////////////////////////////////////
SwerveDrive::SwerveDrive(bool enabled) : m_enabled(enabled)
{
   // Intentionally left empty
}

////////////////////////////////////////////////////////////////////////////////
SwerveDrive::~SwerveDrive(void)
{
   //@TODO delete everything!
}


////////////////////////////////////////////////////////////////////////////////
void 
SwerveDrive::setEnabledState(bool new_state)
{
   m_enabled = new_state;

   // update module states to match
   if (m_enabled == true)
   {
      for (auto module : m_modules)
      {
         module->enable();
      }
   }
   else if (m_enabled == false)
   {
      for (auto module : m_modules)
      {
         module->disable();
      }
   }
}


////////////////////////////////////////////////////////////////////////////////
bool 
SwerveDrive::getEnabledState(void)
{
   return m_enabled;
}


//////////////////////////////////////////////////////////////////////////
void SwerveDrive::resetPIDs(void)
{
   // @TODO
}


//////////////////////////////////////////////////////////////////////////
std::vector<Psb::Drivebase::SwerveModule*> SwerveDrive::getModules(void)
{
   return m_modules;
}


//////////////////////////////////////////////////////////////////////////
void SwerveDrive::addModule(Psb::Drivebase::SwerveModule* new_module)
{
   if (m_enabled)
   {
      new_module->enable();
   }
   else
   {
      new_module->disable();
   }

   
   m_modules.push_back(new_module);
}


//////////////////////////////////////////////////////////////////////////
void SwerveDrive::configureStandardSetup(double wheelbase, double trackwidth, PsbPidController* pids[4])
{
   //@TODO
}


//////////////////////////////////////////////////////////////////////////
void SwerveDrive::setRotateOrigin(double x, double y)
{
   m_rotate_origin[0] = x;
   m_rotate_origin[1] = y;
}


////////////////////////////////////////////////////////////////////////////////
void
SwerveDrive::set(double STR, double FWD, double RCW, double GYRO)
{
   

   int numModules = m_modules.size();
  
   // if disabled or no modules
   if (!m_enabled || numModules <= 0)
   {
      // intentionally left empty

      return;
   }
  
   

  
  

   // if field centric then rotate the translate vector 
   // Note: this code expects the gyro to be positive clockwise
   if (GYRO != NULL)
   {
      double GRYO_radi = (M_PI /180) * DegreeCalc::normalizeDegree(GYRO);// cpp takes radians in cos, sin, and etc. So convert it to radians

      double temp = FWD * cos(GRYO_radi) + STR * sin(GRYO_radi);
      STR = -FWD * sin(GRYO_radi) + STR * cos(GRYO_radi);
      FWD = temp;


     
   }

  // std::cout << "STR:" << STR << " FWD:" << FWD << " RCW:" << RCW << " GYRO:" << GYRO << std::endl;


     
   double max_drive_speed = 0;

   std::vector<std::vector<double>> temp_results;

   
   for (auto module : m_modules)
   {

      // get module location relative to rotate origin
      double moduleX = module->getX() - m_rotate_origin[0];
      double moduleY = module->getY() - m_rotate_origin[1];


      double r = sqrt(pow(moduleX, 2) + pow(moduleY, 2)) / 2;



      double theta_y = RCW * (moduleX / r);
      double theta_x = RCW * (moduleY / r);

     


      double complete_vector [] = {theta_y + FWD, theta_x + STR};

      // check max_drive_speed with new speed because they have to be relative to eachother
      double new_speed = sqrt(pow(complete_vector[0], 2) + pow(complete_vector[1], 2));
      if (new_speed > max_drive_speed)
      {
         max_drive_speed = new_speed;
      }

      double new_heading = atan2(complete_vector[1], complete_vector[0] * -1) * 180 / M_PI;


      temp_results.push_back({new_speed, new_heading});



      
      

   }

   // normalize it if need be
   double divider = 1;
   if (max_drive_speed > 1)
   {
      divider = max_drive_speed;
   }

   for (int i = 0; i < numModules; i++)
   {
      
      m_modules[i]->setVelocity(temp_results[i][0]/divider, temp_results[i][1]);

   
     // std::cout<< "  " + std::to_string(i) + ":" + std::to_string(m_modules[i]->getCurrentAngle(false)) + ">" + std::to_string(temp_results[i][1]);
   }

 //  std::cout << std::endl;

   
   

}












} // END namespace Drivebase
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////