// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include "../../RobotSimulator/b3RobotSimulatorClientAPI.h"
#include "../Utils/b3Clock.h"
#include "common/Gamepad.h"
#include "BSwerveDrive.h"
#include <string.h>
#include <iostream>

#include <assert.h>
#include <filesystem>
#include <math.h>

#include <windows.h>
#include "shlwapi.h"
#include <xinput.h>
#include "TestySim.h"
#include "SwerveRobot.h"





// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////
#define ASSERT_EQ(a, b) assert((a) == (b));//@INVESTIGATE Is this needed?





int main(int argc, char* argv[])
{

	b3RobotSimulatorClientAPI* sim = new b3RobotSimulatorClientAPI();
	bool isConnected = sim->connect(eCONNECT_GUI);

	if (!isConnected)
	{
		std::cout << ("Cannot connect to physics server, stopping...") << std::endl;
		return -1;
	}



	//hide gui 
	//Can also use eCONNECT_DIRECT,eCONNECT_SHARED_MEMORY,eCONNECT_UDP,eCONNECT_TCP, for example:
	//sim->connect(eCONNECT_UDP, "localhost", 1234);
	sim->configureDebugVisualizer(COV_ENABLE_GUI, 0);
	
	//	sim->configureDebugVisualizer( COV_ENABLE_SHADOWS, 0);//COV_ENABLE_WIREFRAME
	sim->setTimeOut(10);
	//syncBodies is only needed when connecting to an existing physics server that has already some bodies
	sim->syncBodies();
	btScalar fixedTimeStep = 1. / 240.;

	sim->setTimeStep(fixedTimeStep);

	btQuaternion q = sim->getQuaternionFromEuler(btVector3(0.1, 0.2, 0.3));
	btVector3 rpy;
	rpy = sim->getEulerFromQuaternion(q);

	sim->setGravity(btVector3(0, 0, -9.8));

	//int blockId = sim->loadURDF("cube.urdf");
	//b3BodyInfo bodyInfo;
	//sim->getBodyInfo(blockId,&bodyInfo);



//	sim->loadURDF("plane.urdf");
	/*
	MinitaurSetup minitaur;
	int minitaurUid = minitaur.setupMinitaur(sim, btVector3(0, 0, .3));
*/




//@INVESTIGATE what is this doing?
/*
	b3Clock clock;
	double startTime = clock.getTimeInSeconds();
	double simWallClockSeconds = 20.;
#if 0
	while (clock.getTimeInSeconds()-startTime < simWallClockSeconds)
	{
		sim->stepSimulation();
	}
#endif*/

	////////////////////////////////////////////////////////
	// Figure out our path
	
	#ifndef UNICODE  
 		typedef std::string String; 
	#else
  		typedef std::wstring String; 
	#endif


	// Get the full path of current exe file.
	TCHAR FilePath[MAX_PATH] = { 0 };
	GetModuleFileName( 0, FilePath, MAX_PATH );

	// Strip the exe filename from path and get folder name.
	PathRemoveFileSpec( FilePath );    

	std::string filePath = FilePath;// Ignore intellisense on this line!

	std::cout << "Program Location: " << filePath.c_str() << std::endl;


	sim->setAdditionalSearchPath(filePath);


	TestySim* ts = new TestySim(sim);
	ts->loadField();

	for (int x = 0; x < 1; x++)
	{
		SwerveRobot* sr = new SwerveRobot();
		sr->configure(sim, btVector3(3, 0, x + 1), btQuaternion(0, 1.5, 0));



		ts->addRobot(sr);
	}


	ts->go(fixedTimeStep);
	

/*


	





	////////////////////////////////////////////////////
	// Do our simulation
	sim->setRealTimeSimulation(false);
	bool field_centric_driving = true;

	while (sim->canSubmitCommand())
	{

	



		





		sim->stepSimulation();

		
		// make the camera follow the robot
		b3OpenGLVisualizerCameraInfo* camer_info = new b3OpenGLVisualizerCameraInfo();
		sim->getDebugVisualizerCamera(camer_info);
		sim->resetDebugVisualizerCamera(camer_info->m_dist, camer_info->m_pitch, camer_info->m_yaw, robot_position);
		




		b3Clock::usleep(1000. * 1000. * fixedTimeStep);
	}
	*/

	std::cout << ("sim->disconnect") << std::endl;

	sim->disconnect();

	std::cout << ("delete sim") << std::endl;
	delete sim;

	std::cout << ("exit") << std::endl;
}






