// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2013-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#include <math.h>
#include <cmath> // needed for std::fmod()
#include "PsbPidController.h"

////////////////////////////////////////////////////////////////////////////////
// @brief
// The PsbPidController class implements a classic PID controller with the
//  following modifications:
//  1.)  Integral windup is addressed via the inner and outer eplison config
//       parameters.  The integral term is allowed to contribute to the output
//       once |e(t)| < outerEpsilon and it is zero'd out once |e(t)| < innerEpsilon.
//       InnerEpsilon is also the dead-band region.
//  2.)  The derivative of the pv(t) is used instead of e(t).  In this way,
//       large changes in the setPoint do not result in a jumpy system.
//  3.)  ??
////////////////////////////////////////////////////////////////////////////////
PsbPidController::PsbPidController(float kp,
                                   float ki,
                                   float kd,
                                   float setPoint,
                                   float minInput,       float maxInput,
                                   float minOutput,      float maxOutput,
                                   float minIntegralSum, float maxIntegralSum,
                                   float innerEpsilon,   float outerEpsilon, bool continuous)
   : a_kp(kp)
   , a_ki(ki)
   , a_kd(kd)
   , a_setPoint(setPoint)
   , a_minInput(minInput)
   , a_maxInput(maxInput)
   , a_minOutput(minOutput)
   , a_maxOutput(maxOutput)
   , a_minIntegralSum(minIntegralSum)
   , a_maxIntegralSum(maxIntegralSum)
   , a_innerEpsilon(innerEpsilon)
   , a_outerEpsilon(outerEpsilon)
   , a_isEnabled(false)
   , a_isOnTarget(false)
   , a_currentProcessValue(0.0f)
   , a_previousProcessValue(0.0f)
   , a_integralSum(0.0f)
   , a_continuous(continuous)
   , a_inputRange(maxInput - minInput)
{
   // Epsilons must be non-negative
   a_innerEpsilon = fabs(a_innerEpsilon);
   a_outerEpsilon = fabs(a_outerEpsilon);
   
   // Quick sanity checks
   // @TODO: Redo these
   //if (a_minInput  > a_maxInput)            { WS_LOG_ERROR("Invalid PID Config: %f, %f", a_minInput, a_maxInput); }
   //if (a_minOutput > a_maxOutput)           { WS_LOG_ERROR("Invalid PID Config: %f, %f", a_minOutput, a_maxOutput); }
   //if (a_minIntegralSum > a_maxIntegralSum) { WS_LOG_ERROR("Invalid PID Config: %f, %f", a_minIntegralSum, a_maxIntegralSum); }
   //if (a_setPoint < a_minInput)             { WS_LOG_ERROR("Invalid PID Config: %f, %f", a_setPoint, a_minInput); }
   //if (a_setPoint > a_maxInput)             { WS_LOG_ERROR("Invalid PID Config: %f, %f", a_setPoint, a_maxInput); }
   //if (a_innerEpsilon > a_outerEpsilon)     { WS_LOG_ERROR("Invalid PID Config: %f, %f", a_innerEpsilon, a_outerEpsilon); }
}

PsbPidController::~PsbPidController(void)
{
}

void PsbPidController::enable(void)
{
   a_isEnabled = true;
   return;
}

void PsbPidController::disable(void)
{
   a_isEnabled = false;
   return;
}

void PsbPidController::setSetPoint(float setPoint)
{
   a_setPoint = setPoint;
   return;
}

void PsbPidController::setConfiguration(float kp,
                                        float ki,
                                        float kd,
                                        float setPoint,
                                        float minInput,       float maxInput,
                                        float minOutput,      float maxOutput,
                                        float minIntegralSum, float maxIntegralSum,
                                        float innerEpsilon,   float outerEpsilon, bool continuous)
{
   a_kp = kp;
   a_ki = ki;
   a_kd = kd;
   a_setPoint = setPoint;
   a_minInput = minInput;
   a_maxInput = maxInput;
   a_minOutput = minOutput;
   a_maxOutput = maxOutput;
   a_minIntegralSum = minIntegralSum;
   a_maxIntegralSum = maxIntegralSum;
   a_innerEpsilon = innerEpsilon;
   a_outerEpsilon = outerEpsilon;
   a_continuous = continuous;
   a_inputRange = maxInput - minInput;
   return;
}

float PsbPidController::calcPid(float processValue)
{
   // Stack variables for the PID calculations
   float output = 0.0f;
   float error  = 0.0f;
   float pterm  = 0.0f;
   float iterm  = 0.0f;
   float dterm  = 0.0f;

   // Default to not on target
   a_isOnTarget = false;
   
   // We have work to do if the PID Controller is enabled
   if (true == a_isEnabled)
   {
      // Shift the process variable values
      a_previousProcessValue = a_currentProcessValue;
      a_currentProcessValue  = processValue;
      
      // Clip the processValue via min and max inputs
      a_currentProcessValue = (a_currentProcessValue < a_minInput) ? a_minInput : a_currentProcessValue;
      a_currentProcessValue = (a_currentProcessValue > a_maxInput) ? a_maxInput : a_currentProcessValue;
      
      // Calculate the current e(t)
      error = a_setPoint - a_currentProcessValue;
      
      // Are we currently on target?
      if (fabs(error) < a_innerEpsilon)
      {
         // We're on target!
         output        = 0.0f;
         a_integralSum = 0.0f;
         a_isOnTarget  = true;
      }
      else
      {
         // Nope, still have work to do

         //---------------------------------------------------------------------------------------
         // If need be Calculate error for continuous, NOTE: this has not been tested thoroughly
         //---------------------------------------------------------------------------------------
         if (a_continuous)
         {
          
            error = std::fmod(error, a_inputRange);

            if (fabs(error) > a_inputRange / 2.0) 
            {


               if (error > 0.0) 
               {
                   error -= a_inputRange;
               } 
               else 
               {
                   error += a_inputRange;
               }
            }
        }
         
         //------------------------------------------------------------------------
         // Calculate the Proportional contribution, easy
         //------------------------------------------------------------------------
         pterm = a_kp * error;
         
         //------------------------------------------------------------------------
         // Calculate the Integral Term
         //------------------------------------------------------------------------
         if (fabs(error) < a_outerEpsilon)
         {
            // We are close enough to the target to let the I-term wind up
            a_integralSum += error;
            a_integralSum = (a_integralSum < a_minIntegralSum) ? a_minIntegralSum : a_integralSum;
            a_integralSum = (a_integralSum > a_maxIntegralSum) ? a_maxIntegralSum : a_integralSum;
         }
         else
         {
            // We are so far away from the target, the I-term will overshoot
            a_integralSum = 0.0f;
         }
         iterm = a_ki * a_integralSum;
         
         //------------------------------------------------------------------------
         // Calculate the Differential Term
         //------------------------------------------------------------------------
         dterm = a_kd * (a_currentProcessValue - a_previousProcessValue);
         
         //------------------------------------------------------------------------
         // Putting it all together...
         //------------------------------------------------------------------------
         output = pterm + iterm - dterm;
         
         //------------------------------------------------------------------------
         // And checking for proper direction...
         //  If the setpoint is above us, make sure we aren't moving the other way.
         //  If we are moving the wrong way, make sure we kill the I-term.
         //------------------------------------------------------------------------
         if (error >= 0.0f)
         {
            output        = (output >= 0.0f) ? output        : 0.0f;
            a_integralSum = (output >= 0.0f) ? a_integralSum : 0.0f;
            
         }
         else
         {
            output        = (output < 0.0f) ? output        : 0.0f;
            a_integralSum = (output < 0.0f) ? a_integralSum : 0.0f;
         }
      }
      
      // Clip the output value according to min/max config
      output = (output < a_minOutput) ? a_minOutput : output;
      output = (output > a_maxOutput) ? a_maxOutput : output;
      
   }
   else
   {
      // The PID Controller is currently disabled, output should be nil
      output = 0.0f;
   }
   
   return output;
}

bool PsbPidController::isOnTarget(void)
{
   return a_isOnTarget;
}

