#include "BSwerveDrive.h"
#include "../../SharedMemory/b3RobotSimulatorClientAPI_NoGUI.h"
#include <vector>
#include "swerve/SwerveModule.h"
#include "common_sim/RobotSimulator.h"

#include "../../../src/Bullet3Common/b3HashMap.h"



////////////////////////////////////////////////////////////////////////////////
BSwerveDrive::BSwerveDrive()
{

}


////////////////////////////////////////////////////////////////////////////////
BSwerveDrive::BSwerveDrive(class b3RobotSimulatorClientAPI_NoGUI* sim, int uniqueId, std::vector<BSwerveModuleArgs> modules) 
	:
	m_uniqueId(uniqueId),
	m_sd_controller(new Psb::Drivebase::SwerveDrive())
{
	 


	std::map<std::string, int> index_Map = RobotSimulator::mapNameToId(sim, m_uniqueId);


	// figure out ids to each swerve module
	for (int i = 0; i < modules.size(); i++)
	{
		
		
		int drive_index = index_Map[modules[i].m_drive_joint_name];
		int rotate_index = index_Map[modules[i].m_rotate_joint_name];

		// figure out this modules location from the rotate joint
		b3JointInfo* info = new b3JointInfo();
		sim->getJointInfo(m_uniqueId, rotate_index, info);


		double x = info->m_parentFrame[0];
		double y = info->m_parentFrame[1];		
		double z = info->m_parentFrame[2];


		printf("mapping modules - pos(%.3f, %.3f, %.3f) index(rotate(%.d), drive(%.d))\n", x, y, z, rotate_index, drive_index);

		Psb::Drivebase::SwerveModule* sm = new Psb::Drivebase::SwerveModule(x, z, modules[i].m_rotate_pid);
		sm->setTickValueForForward(270);//DEBUG purposes only!!
		m_sd_controller->addModule(sm);

		// create struct to keep track of the controller and indexes for joints of this module
		BSwerveModule sdm = BSwerveModule(sm, rotate_index, drive_index, modules[i].m_motor_max_velocity, modules[i].m_motor_max_torque);
	

		m_modules.push_back(sdm);
	}
}


////////////////////////////////////////////////////////////////////////////////
BSwerveDrive::~BSwerveDrive()
{
	//@TODO delete everything!
}







////////////////////////////////////////////////////////////////////////////////
void
BSwerveDrive::applySwerveControllerOuputs(class b3RobotSimulatorClientAPI_NoGUI* sim)
{
	for (int i = 0; i < m_modules.size(); i++)
	{
		m_modules[i].applySwerveControllerOuputs(sim, m_uniqueId);
	}
}

////////////////////////////////////////////////////////////////////////////////
void
BSwerveDrive::getSwerveControllerInputs(class b3RobotSimulatorClientAPI_NoGUI* sim)
{
	for (int i = 0; i < m_modules.size(); i++)
	{
		m_modules[i].getSwerveControllerInputs(sim, m_uniqueId);
	}
}





////////////////////////////////////////////////////////////////////////////////
Psb::Drivebase::SwerveDrive* 
BSwerveDrive::getSwerveDrive(void)
{
	return m_sd_controller;

}


