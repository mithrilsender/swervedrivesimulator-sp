// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include "../../../RobotSimulator/b3RobotSimulatorClientAPI.h"
#include "../Utils/b3Clock.h"
#include "common/Gamepad.h"
#include <string.h>
#include <iostream>

#include <assert.h>
#include <filesystem>
#include <math.h>

#include <windows.h>
#include "shlwapi.h"
#include <xinput.h>

#include "SwerveRobot.h"
#include "BSwerveDrive.h"
#include "common_sim/Robot.h"
#include "common_sim/RobotSimulator.h"


//////////////////////////////////////////////////////////////////////////
SwerveRobot::SwerveRobot(void)
{
	// intentionally left empty
}


//////////////////////////////////////////////////////////////////////////
void 
SwerveRobot::runTasks(class b3RobotSimulatorClientAPI_NoGUI* sim)
{

	// get virtual bullet physics inputs for the sd
	m_bsdController->getSwerveControllerInputs(sim);



	//////////////////////////////////////////////////////////
	// get gyro angle
	btVector3 robot_position;
	btQuaternion robot_orientation;
	sim->getBasePositionAndOrientation(m_id, robot_position, robot_orientation);


	// get euler of orientation
	btScalar robot_yaw;
	btScalar robot_pitch;
	btScalar robot_roll;
	robot_orientation.getEulerZYX(robot_yaw, robot_pitch, robot_roll);

	double gyro_angle = 360 - ( robot_yaw * 57.296 + 180);// convert to degrees, stop it from going -180 to 180, reverse it so its clockwise
	//printf("gryo:%.3f\n", gyro_angle);


	///////////////////////////////////////////////////////////
	// Get oi Inputs for our simulation
	XINPUT_STATE state = m_gamepad->getState();
 
	if (XInputGetState(m_gamepad->getConnectedId(), &state) == ERROR_SUCCESS)// if still connected
	{
		// Extract raw datad
		float normLX = fmaxf(-1, (float) state.Gamepad.sThumbLX / 32767);
		float normLY = fmaxf(-1, (float) state.Gamepad.sThumbLY / 32767);

		float normRX = fmaxf(-1, (float) state.Gamepad.sThumbRX / 32767);
		//float normRY = fmaxf(-1, (float) state.Gamepad.sThumbRY / 32767);
		


		// apply dead zones
		float leftStickX = Gamepad::applyDeadZone(normLX, m_analog_stick_deadzone);
		float leftStickY = Gamepad::applyDeadZone(normLY, m_analog_stick_deadzone);
		
		float rightStickX = Gamepad::applyDeadZone(normRX, m_analog_stick_deadzone);


	


		
		
		
		// toggle field centric if needed 
		if (((state.Gamepad.wButtons & XINPUT_GAMEPAD_A) != 0))
		{
			if (m_field_centric_driving)
			{
				std::cout << "Switching to robot centric driving" << std::endl;
				m_field_centric_driving = false;
			}					
			else if (!m_field_centric_driving)
			{
				m_field_centric_driving = true;
				std::cout << "Switching to field centric driving" << std::endl;
			}
				
		}


		// apply controller output
		m_sd_controller->set(leftStickX, leftStickY, rightStickX, (m_field_centric_driving == true) ? gyro_angle : NULL);

		
	}
	else
	{
		m_gamepad->connect();
		m_sd_controller->set(0, 0, 0);
	}




	
		
	m_bsdController->applySwerveControllerOuputs(sim);


	
}

//////////////////////////////////////////////////////////////////////////
void SwerveRobot::configure(class b3RobotSimulatorClientAPI_NoGUI* sim, btVector3 starting_location, btQuaternion starting_orientation)
{


	/////////////////////////////////////////////////////////
	// Create the Swerve drive
	const int num_modules = 4;
	
	// define the swerve drive spawn
	b3RobotSimulatorLoadUrdfFileArgs load_args;
	load_args.m_startOrientation = starting_orientation;
	load_args.m_startPosition = starting_location;

	m_id = sim->loadURDF("swerve_robot_2.urdf", load_args);





	// define the names of the module motors in our urdf and their associated pid controller
	std::vector<BSwerveModuleArgs> modules;
	for (int i = 0; i < num_modules; i++)
	{
		PsbPidController* rotate_pid = Psb::Drivebase::SwerveModule::createPID(0.01, 0, 0, -1.0, 1.0, 1, 10, 0.1, 6);


		modules.push_back(BSwerveModuleArgs(std::to_string(i) + "_SM_motor_rotate", std::to_string(i) + "_SM_motor_drive", 60, 60, rotate_pid));
	}
	


	// create bullet physics swerve drive controller
	m_bsdController = new BSwerveDrive(sim, m_id, modules);
	m_sd_controller = m_bsdController->getSwerveDrive();
	m_sd_controller->setEnabledState(true);
	// sd_controller->setRotateOrigin(2, 0);


	std::map<std::string, int> robot_map = RobotSimulator::mapNameToId(sim, m_id);


	// increase friction on wheels of our robot
	/*
	for (int i = 0; i < 4; i++)
	{
		int wheelId = robot_map[std::to_string(i) + "_SM_wheel"];

		// get current dynamics
		b3DynamicsInfo* get_info = new b3DynamicsInfo();
		sim->getDynamicsInfo(m_id, wheelId, get_info);
		printf("contactDamping:%.3f", get_info->m_contactDamping);
		

		b3RobotSimulatorChangeDynamicsArgs args;
     	
		args.m_contactStiffness = 1;
		
		//args.m_contactDamping = -10000;
	    args.m_rollingFriction = 1000;
		sim->changeDynamics(m_id, wheelId, args);
		
	}*/


	// setup suspension	
	for (int i = 0; i < num_modules; i++)
	{
		int susId = robot_map[std::to_string(i) + "_SM_suspension"];

		b3RobotSimulatorJointMotorArgs args = b3RobotSimulatorJointMotorArgs(2);
		args.m_targetPosition = -0.05;
		
		sim->setJointMotorControl(m_id, susId, args);
		
	}



	///////////////////////////////////////////////////////
	// Configure OI

	// Find a controller with xinput
	m_gamepad = new Gamepad();
	m_gamepad->connect();

	

	// button example: bool A_button_pressed = ((state.Gamepad.wButtons & XINPUT_GAMEPAD_A) != 0);
	
}

   

