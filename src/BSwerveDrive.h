#ifndef B_SWERVE_DRIVE_H
#define B_SWERVE_DRIVE_H

#include "LinearMath/btVector3.h"
#include "../../src/LinearMath/btQuaternion.h"
#include "../../../src/Bullet3Common/b3HashMap.h"
#include "../../SharedMemory/b3RobotSimulatorClientAPI_NoGUI.h"
#include <vector>
#include <map>
#include "swerve\SwerveModule.h"
#include "swerve\SwerveDrive.h"



struct BSwerveModuleArgs
{
	public:


		// urdf name for joints that will act as our motors
		std::string m_rotate_joint_name;
		std::string m_drive_joint_name;


		// motor data
		double m_motor_max_velocity;
		double m_motor_max_torque;


		PsbPidController* m_rotate_pid;


		BSwerveModuleArgs(
			std::string rotate_joint_name,
			std::string drive_joint_name,
			double motor_max_velocity,
			double motor_max_torque,
			PsbPidController* rotate_pid)
			:
			m_rotate_joint_name(rotate_joint_name),
			m_drive_joint_name(drive_joint_name),
			m_motor_max_torque(motor_max_torque),
			m_motor_max_velocity(motor_max_velocity),
			m_rotate_pid(rotate_pid)
		{};

};



struct BSwerveModule
{
	public:


		// linking info
		int m_rotate_joint_index;
		int m_drive_joint_index;


		// our pointer to the controller of this module
		Psb::Drivebase::SwerveModule* m_module;



		// motor data
		double m_motor_max_velocity;
		double m_motor_max_torque;
		

		// gets angles from bullet physics for our motors and use those as encoders
		void getSwerveControllerInputs(class b3RobotSimulatorClientAPI_NoGUI* sim, int bodyId)
		{	
			// get drive info
			//@TODO add support for this maybe? will need it for auto

			// get rotate info
			b3JointSensorState * rotate_info = new b3JointSensorState();
			sim->getJointState(bodyId, m_rotate_joint_index, rotate_info);
	

			m_module->setRotateEncoderCounts(/*360 -*/ (rotate_info->m_jointPosition * 57.2957795131)); // convert radien to 360 degrees and swap it to be clockwise
			if (m_rotate_joint_index == 7)
			{
				printf("module%d%d angle %.3f -> %.3f rotate:%.3f drive:%.3f\n", m_rotate_joint_index, m_drive_joint_index, m_module->getCurrentAngle(), m_module->getDesiredAngle(), m_module->getRotateMotor(), m_module->getDriveMotor());
			}
		
		};


		// applies m_modules result to the corresponding bullet physics joints
		void applySwerveControllerOuputs(class b3RobotSimulatorClientAPI_NoGUI* sim, int bodyId)
		{
			
			
			// set drive motor
			b3RobotSimulatorJointMotorArgs drive_args = b3RobotSimulatorJointMotorArgs(0);
			drive_args.m_targetVelocity = m_module->getDriveMotor()*m_motor_max_velocity;
			drive_args.m_maxTorqueValue = m_motor_max_torque;

			sim->setJointMotorControl(bodyId, m_drive_joint_index, drive_args);




			// set rotate motor
			b3RobotSimulatorJointMotorArgs rotate_args = b3RobotSimulatorJointMotorArgs(0);
			rotate_args.m_targetVelocity = m_module->getRotateMotor()*m_motor_max_velocity;
			rotate_args.m_maxTorqueValue = m_motor_max_torque;

			sim->setJointMotorControl(bodyId, m_rotate_joint_index, rotate_args);

		//	if (m_rotate_joint_index == 0 && m_drive_joint_index == 2)
		//	printf("module%d%d - drive:%.3f rotate:%.3f\n", m_rotate_joint_index, m_drive_joint_index, m_module->getDriveMotor(), m_module->getRotateMotor());
		};




		BSwerveModule(Psb::Drivebase::SwerveModule* module, int rotate_joint_id, int drive_joint_id, double motor_max_velocity, double motor_max_torque) : 
			m_rotate_joint_index(rotate_joint_id),
			m_drive_joint_index(drive_joint_id),
			m_module(module),
			m_motor_max_torque(motor_max_torque),
			m_motor_max_velocity(motor_max_velocity)
		{};
		
};


class BSwerveDrive
{
	
	
private:
	std::vector<BSwerveModule> m_modules;

	int m_uniqueId = -1;

	
	Psb::Drivebase::SwerveDrive* m_sd_controller;


public:



	BSwerveDrive();
	BSwerveDrive(class b3RobotSimulatorClientAPI_NoGUI* sim, int uniqueId, std::vector<BSwerveModuleArgs> modules);


	virtual ~BSwerveDrive();

	void applySwerveControllerOuputs(class b3RobotSimulatorClientAPI_NoGUI* sim);

	void getSwerveControllerInputs(class b3RobotSimulatorClientAPI_NoGUI* sim);
	

	Psb::Drivebase::SwerveDrive* getSwerveDrive(void);








	

	




	


};


#endif  //B_SWERVE_DRIVE_H