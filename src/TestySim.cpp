// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////

#include "../Utils/b3Clock.h"
#include <string.h>
#include <iostream>

#include <assert.h>
#include <filesystem>
#include <math.h>

#include <windows.h>
#include "shlwapi.h"
#include <xinput.h>

#include "TestySim.h"




//////////////////////////////////////////////////////////////////////////
TestySim::TestySim(class b3RobotSimulatorClientAPI_NoGUI* sim) : RobotSimulator(sim)
{

}



//////////////////////////////////////////////////////////////////////////
void
TestySim::loadField(void)
{

	b3RobotSimulatorLoadUrdfFileArgs field_load_args;
	field_load_args.m_startOrientation = btQuaternion(0, 0, 0);
	field_load_args.m_startPosition = btVector3(0, 0, 0);

	m_simClient->loadURDF("plane.urdf", field_load_args);



	//int frc_fieldId = m_simClient->loadURDF("frc2018field/frc2018field.urdf", field_load_args);

	// fix scales for 2018 map
	/*
	std::map<std::string, int> field_map = RobotSimulator::mapNameToId(m_simClient, frc_fieldId);

	for (int x = 0; x < 1; x++)
	{
		b3JointInfo* info = new b3JointInfo();
		info->m_jointFriction = 0;
		info->m_jointDamping = 0;
		info->m_jointIndex = field_map[std::to_string(x) + "_switch_joint"];
		
		m_simClient->changeConstraint(frc_fieldId, info);
		

				b3RobotSimulatorJointMotorArgs* info = new b3RobotSimulatorJointMotorArgs(1);
		info->

		m_simClient->setJointMotorControl(frc_fieldId,  field_map[std::to_string(x) + "_switch_joint"], )
	}*/

	
	// load some props
	for (int x = 0; x < 6; x++)
	{
		b3RobotSimulatorLoadUrdfFileArgs cube_load_args;
		cube_load_args.m_startOrientation = btQuaternion(0, 1.5, 0);
		cube_load_args.m_startPosition = btVector3(0, 3, 4 + x);


		int power_cubeId = m_simClient->loadURDF("power_cube.urdf", cube_load_args);

	}

}



//////////////////////////////////////////////////////////////////////////
void 
TestySim::go(void)
{

	go(1. / 240.);
	

}


//////////////////////////////////////////////////////////////////////////
void 
TestySim::go(double fixedTimeStep)
{
	m_simClient->setRealTimeSimulation(false);
	bool field_centric_driving = true;



	while (m_simClient->canSubmitCommand())
	{

		runRobotTasks();


		m_simClient->stepSimulation();



		b3Clock::usleep(1000. * 1000. * fixedTimeStep);
	}
}



