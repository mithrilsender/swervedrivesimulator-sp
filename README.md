
My purpose in this codebase is to simulate a functioning swerve drive. To accomplish this I'm creating a virtual swerve drive in bullet physics and gathering joystick inputs using XInput which unfortunately means that this will only work on windows. The code that actually does the calculations for the swerve drive are located under the src/swerve directory.


Requirements:
 - Windows operating system greater >= 8
 - ~1.3 ish gigabytes of free space (for the bullet physics repo)


Setup:
 - Get a c++ comiler (personally I've found the easiest thing is to simply download visual studio and install their c++ compiler within the visual studio installer) @TODO find which c++ version I'm using

 - Get Cmake from here https://cmake.org/download/ for windows

 - Get BulletPhysics. Quick Explaination, as I understand it currently the authors of bullet physics have been focusing on there python API which combined with not having enough people has resulted in many features existing in the examples directory for the sake of convience. This unfortunately means that in order to use those features we have to jump through some hoops (For the record this is still probably not the best way of doing it). So Download the bullet physics repo (via zip, git, etc) here https://github.com/bulletphysics/bullet3. Then move the swervedrivesimulator-sp directory obtained by downloading this repo to name_of_the_root_directory_of_bullet_physics\examples\swervedrivesimulator-sp. Lastly edit name_of_the_root_directory_of_bullet_physics\examples\CMakeLists.txt and add the text 'drivesimulator-sp' to the SUBDIR() parentheses. Now if you configure and build with cmake from the name_of_the_root_directory_of_bullet_physics directory this repo should also build.

