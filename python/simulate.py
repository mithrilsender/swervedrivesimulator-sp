import pybullet as p
import time
import pybullet_data
import math
#import swervedrivewrapper as sdw # our custom dll/pyd file built from cpp code
import os, inspect
#from HelperClasses import *
#from inputgrabbers import GamepadGrabber







def end():
	print("!ENDING SCRIPT!")

	# quietly kill the program 
	import sys
	sys.exit()


# prints out all info about modules
def printModules(sdh):
	moduleNum = sdh.getNumModules()
	print("Module#:" + str(moduleNum))


	for x in range(moduleNum):

		module = sdh.getModule(x)
		print(" ")
		print("Module#" + str(x) + " @" + Point(module.getX(), module.getY()).string())
		
		print("Drive:" + str(module.getDriveMotor()))
		print("Rotate:" + str(module.getRotateMotor()))


# maps id to string name (name comes from the urdf file)
def mapJoints(pysClient, id):
	jointIdMap = {}

	jointNumb = pysClient.getNumJoints(id)

	print("mapping joints...")
	for x in range(jointNumb):
		info = pysClient.getJointInfo(id, x)
		name = info[1].decode("utf-8") 

		#map it
		jointIdMap[name] = int(x)

		print("(" + str(x) + ")" + name)

	return jointIdMap



# Configure the Swerve Drive
#module1 = (sdw.SwerveModule(0, 1, 0.5, 0.0, 0.0))


#sdh.configureStandardSetup(1, 1)
#print("Created SwerveDrive!")
'''sdh = sdw.SwerveDriveHelper(True)
module = sdw.SwerveModule(1, 1, 0.5, 0.0, 0.0)
sdh.sd.addModule(module)

printModules(sdh)
end()'''


# configure the inputs
#gamepad = GamepadGrabber()
#gamepad.startAsyncUpdates()


# left joystick
#gamepad.getValue("ABS_Y")/gamepad.maxAxisValue
#gamepad.getValue("ABS_X")/gamepad.maxAxisValue

# right joystick
#gamepad.getValue("ABS_RY")/gamepad.maxAxisValue
#gamepad.getValue("ABS_RX")/gamepad.maxAxisValue



# figure out directories for loading stuff
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
print("current directory = " + currentdir)
modelsdir = currentdir #+ "/models"





physicsClient = p.connect(p.GUI)#or p.DIRECT for non-graphical version
p.configureDebugVisualizer(p.COV_ENABLE_GUI,0)#disable extra ui stuff
p.setGravity(0,0,-10)


# import urdfs from pybullet_data
p.setAdditionalSearchPath(pybullet_data.getDataPath())
#planeId = p.loadURDF("plane.urdf")#produces small plane
#p.createCollisionShape(p.GEOM_PLANE) # using these two lines instead yields really big plane
#p.createMultiBody(0, 0)

# import urdfs from our models dir

p.setAdditionalSearchPath(modelsdir)

cubeStartPos = [0,0,0]
cubeStartOrientation = p.getQuaternionFromEuler([1.5708,0,0])
robotId = p.loadURDF(("../models/frc2018field/frc2018field.urdf"), cubeStartPos, cubeStartOrientation)

cubeStartPos = [0,0,10]
cubeStartOrientation = p.getQuaternionFromEuler([1.5708,0,0])
robotId = p.loadURDF(("../models/swerve_robot_2.urdf"), cubeStartPos, cubeStartOrientation)

#p.loadURDF(("../models/frc2019field.urdf"), [0,10,3], p.getQuaternionFromEuler([0,0,0]))

# map it out!
#jointIdMap = mapJoints(p, robotId)
#print(jointIdMap)

'''
# create modules
module1 = Module(Motor(robotId, jointIdMap["1_SM_motor_rotate"], ), Motor(robotId, jointIdMap["1_SM_motor_drive"]))


# set power
module1.rotate_motor.set(1)
module1.drive_motor.set(0)
'''




# Create Swerve Robot 
'''
module_pos = [Point(1.0, 1.0), Point(99, -99)]

sdr = SwerveDriveRobot(module_pos)

sdr.generateModules([1, 0.2, 1], p)

printModules(sdr.sdh)
end()


'''

#----------------------------------------------------------------------------------------
# do simulation
#p.setRealTimeSimulation(1) this leads to a lot of gitter but simulation wise is probably more realistic
for i in range (10000):
	p.stepSimulation()
	time.sleep(1./240.)


	##############################################################
	# Go and fetch Inputs
	##############################################################

	keys = p.getKeyboardEvents()
	#print(keys)


	##############################################################
	# React to those Inputs
	##############################################################


	# make the camera look at the robot
	if (not(122 in keys)):# if no z

		cubePos, cubeOrn = p.getBasePositionAndOrientation(robotId)#get robot orientation and location

		cameraInfo = p.getDebugVisualizerCamera()
		p.resetDebugVisualizerCamera(cameraDistance=cameraInfo[10], cameraYaw=cameraInfo[8], cameraPitch=cameraInfo[9], cameraTargetPosition=cubePos)

	'''
	# output module orientations
	print("rotate motor degree - " + str(module1.rotate_motor.getAngle(p)))
	print("drive motor degree - " + str(module1.drive_motor.getAngle(p)))


	# apply motors
	module1.apply(p)
	'''

	
	

	


	

	
	



p.disconnect()

