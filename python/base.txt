<?xml version="1.0"?>
<robot name="swerveRobot">


  <link name="base">
    <visual>
      <geometry>
        <box size="0.8 0.1 0.8"/>
      </geometry>
      <origin rpy="0 0 0" xyz="0 0 0"/>
      <material name="gray">
        <color rgba="0.9 0.9 0.9 1"/>
      </material>
    </visual>
    <collision>
      <geometry>
        <box size="0.8 0.1 0.8"/>
      </geometry>
      <origin rpy="0 0 0" xyz="0 0 0"/>
    </collision>
    <inertial>
       <mass value="54"/><!-- 54 -->  
       <origin rpy="0 0 0" xyz="0 0 0"/>
      <inertia ixx="1.0" ixy="0.0" ixz="0.0" iyy="1.0" iyz="0.0" izz="1.0"/>
    </inertial>
  </link>

MODULES


</robot>